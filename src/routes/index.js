import React from "react";
import { Routes, Route } from "react-router-dom";
import Home from "../pages/Home/Home";
import Product from "../pages/product";
import Cart from "../pages/cart";
import Profile from "../pages/Services/Services";
import Productdetails from "../pages/product/details";

const Routing = () => {
  return (
    <Routes>
      <Route path="/" element={<Home />} />
      <Route path="/cart" element={<Cart />} />
      <Route path="/product" element={<Product />} />
      <Route path="/profile" element={<Profile />} />
      <Route path="/product/:id" element={<Productdetails />} />
    </Routes>
  );
};

export default Routing;
