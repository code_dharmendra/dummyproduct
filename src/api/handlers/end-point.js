const apiEndPoint = {
  product: "/products",
  searchProduct: (query) => `/products/search?q=${query}`,
  getProductById: (id) => `/product/${id}`,

  //cart api
  cart: "/cart",
  getCartById: (id) => `/cart/${id}`,
};
export default apiEndPoint;
