import { apiEndPoint } from "../handlers";
import api from "../handlers/api";

const getCart = async ({ queryKey }) => {
  if (!Array.isArray(queryKey)) {
    throw new Error("queryKey must be an array");
  }
  const [_] = queryKey;
  const params = {};

  const { data } = await api.get(apiEndPoint.cart, {
    params: params,
  });

  return data;
};

const get_cart_by_id = async (id) => {
  const { data } = await api.get(apiEndPoint?.getCartById(id));
  return data;
};

const create_cart = async (value) => {
  const { data } = await api.post(apiEndPoint?.cart, value);
  return data;
};

export { getCart, get_cart_by_id, create_cart };
