import { apiEndPoint } from "../handlers";
import api from "../handlers/api";

const getProduct = async ({ queryKey }) => {
  if (!Array.isArray(queryKey)) {
    throw new Error("queryKey must be an array");
  }
  const [_] = queryKey;
  const params = {};

  const { data } = await api.get(apiEndPoint.product, {
    params: params,
  });

  return data;
};

const get_product_by_id = async (id) => {
  const { data } = await api.get(apiEndPoint?.getProductById(id));
  return data;
};

const get_product_by_search = async (query) => {
  const { data } = await api.get(apiEndPoint?.searchProduct(query));
  return data;
};

const create_product = async (value) => {
  const { data } = await api.post(apiEndPoint?.product, value);
  return data;
};

export { getProduct, get_product_by_id, create_product, get_product_by_search };
