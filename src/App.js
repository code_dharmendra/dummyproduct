import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { Provider } from "react-redux";
import React from "react";
import Routing from "./routes";
import Navbar from "./pages/Navbar/Navbar";
import Footer from "./pages/Footer/Footer";
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import store from "./redux/store";
const App = () => {
  const queryClient = new QueryClient();

  return (
    <div>
      <QueryClientProvider client={queryClient}>
        <Provider store={store}>
          <Navbar />
          <Routing />
          <Footer />
        </Provider>
      </QueryClientProvider>
    </div>
  );
};

export default App;
