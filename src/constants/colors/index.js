const colors = {
  primary: "#3AA2D0",
  secondary: "#2ecc71",
  accent: "#e74c3c",
  background: "#ecf0f1",
  text: "#343434",
  lightAqua: "#E7F2F3",
  darkAqua: "#3a8d9e",
  black: "#222222",
  white: "#ffffff",
  lightGrey: "#979797",
  lightWhite: "#F4F4F4",
  waring: "#ffe62a",
};

export default colors;
