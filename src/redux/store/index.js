import { configureStore } from "@reduxjs/toolkit";
import cartReducer from "../cartSlice";

const loadCartItemsFromStorage = () => {
  const storedItems = localStorage.getItem("cartItems");
  return storedItems ? JSON.parse(storedItems) : [];
};

const store = configureStore({
  reducer: {
    cart: cartReducer,
  },
  preloadedState: {
    cart: { cartItems: loadCartItemsFromStorage() },
  },
});

store.subscribe(() => {
  const { cart } = store.getState();
  localStorage.setItem("cartItems", JSON.stringify(cart.cartItems));
});

export default store;
