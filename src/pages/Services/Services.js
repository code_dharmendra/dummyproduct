import React from "react";
import useDocumentTitle from "../../function/pageTitle/useDocumentTitle";

const Profile = () => {
  useDocumentTitle("Profile 👻");
  return <div>Profile</div>;
};

export default Profile;
