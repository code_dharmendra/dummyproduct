import React from "react";
import useDocumentTitle from "../../function/pageTitle/useDocumentTitle";
import useProduct from "../product/product.hook";
import { Row, Col, Container } from "react-bootstrap";
import ProductCard from "../../components/productCard";

function Home() {
  useDocumentTitle("Home  👻");
  const { isLoading, productData, productRefetch } = useProduct();
  return (
    <Container>
      <div className="py-5">
        <div>
          <h1 className="mb-5 text-center">All Product List</h1>
        </div>
        {!isLoading ? (
          <Row className="gy-4">
            {productData?.products?.map((item, index) => {
              return (
                <Col md={4}>
                  <ProductCard data={item} />
                </Col>
              );
            })}
          </Row>
        ) : (
          <div className="h-100 centered">
            <h1 className="text-center">Loading...</h1>
          </div>
        )}
      </div>
    </Container>
  );
}

export default Home;
