import React from "react";
import styles from "./Footer.module.css";

const Footer = () => {
  return (
    <footer className={` ${styles.footer} `}>
      <p className="">&copy; 2023 Dummy Product Json Api Doc</p>
    </footer>
  );
};

export default Footer;
