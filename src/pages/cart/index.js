import React from "react";
import { connect } from "react-redux";
import { Table, Image, Container } from "react-bootstrap";
import { FaTrash } from "react-icons/fa";
import { generateColorWithOpacity } from "../../function/helper/generateColor";
import colors from "../../constants/colors";
import { removeFromCart, clearCart } from "../../redux/cartSlice";

const Cart = ({ cartItems, removeFromCart, clearCart }) => {
  const handleRemoveItem = (itemId) => {
    removeFromCart(itemId);
  };

  const handleClearCart = () => {
    clearCart();
  };

  const calculateSubtotal = () => {
    return cartItems
      .reduce(
        (subtotal, item) =>
          subtotal + Number(item.price) * Number(item.quantity),
        0
      )
      .toFixed(2);
  };

  return (
    <Container className="mb-5 py-5">
      <div className="d-flex justify-content-between py-5">
        <h2 className="">Your Product in cart</h2>
        {cartItems.length > 0 && (
          <div className="text-center">
            <button className="btn btn-danger" onClick={handleClearCart}>
              Clear Cart
            </button>
          </div>
        )}
      </div>

      <Table className="table-responsive border table pt-2 ">
        <thead className="border text-uppercase">
          <tr className="text-center">
            <th className="border">
              <h5>S.NO</h5>
            </th>
            <th className="border">
              <h5>Image</h5>
            </th>
            <th className="border">
              <h5>Product Name</h5>
            </th>
            <th className="border">
              <h5>Price</h5>
            </th>
            <th className="border">
              <h5>Quantity</h5>
            </th>
            <th className="border">
              <h5>Total</h5>
            </th>
            <th className="border">
              <h5>Remove</h5>
            </th>
          </tr>
        </thead>
        <tbody>
          {cartItems.map((item, index) => (
            <tr
              key={index}
              className=""
              style={{
                backgroundColor: colors.lightWhite,
                textAlign: "center",
              }}
            >
              <td className="border vertileMiddle">
                <h5 className="">{index + 1}</h5>
              </td>
              <td style={{ verticalAlign: "middle" }}>
                <div>
                  <Image
                    src={item?.thumbnail}
                    className="img-fluid rounded-1"
                    width={100}
                    height={40}
                    style={{
                      backgroundColor: generateColorWithOpacity(
                        colors.lightAqua,
                        0.1
                      ),
                      height: 100,
                    }}
                  />
                </div>
              </td>
              <td className="border vertileMiddle">
                <h6>{item?.title}</h6>
              </td>
              <td className="border vertileMiddle">
                <h6>${Number(item?.price).toFixed(2)}</h6>
              </td>
              <td className="border vertileMiddle">
                <div className="centered w-100">{item?.quantity}</div>
              </td>
              <td className="border vertileMiddle">
                <h6>
                  ${(Number(item?.price) * Number(item?.quantity)).toFixed(2)}
                </h6>
              </td>
              <td className="border vertileMiddle">
                <FaTrash
                  size={24}
                  className="cursor-pointer"
                  title="Remove your Item"
                  onClick={() => handleRemoveItem(item.id)}
                />
              </td>
            </tr>
          ))}
          <tr
            className="py-5 bg-info"
            style={{
              backgroundColor: colors.lightWhite,
              textAlign: "center",
              fontWeight: "bold",
            }}
          >
            <td colSpan="5" className="border vertileMiddle">
              <h4>Subtotal Price</h4>
            </td>
            <td>
              <h4>${calculateSubtotal()}</h4>
            </td>
          </tr>
        </tbody>
      </Table>
    </Container>
  );
};

const mapStateToProps = (state) => {
  return {
    cartItems: state.cart.cartItems,
  };
};

export default connect(mapStateToProps, { removeFromCart, clearCart })(Cart);
