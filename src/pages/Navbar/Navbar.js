import React from "react";
import { Link } from "react-router-dom";
import styles from "./Navbar.css";

const Navbar = () => {
  return (
    <nav
      className={` ${styles.navbar} bg-dark py-3 text-center d-flex justify-content-between px-4 `}
    >
      <h4 className="text-white">Dummy Product</h4>
      <ul>
        <li>
          <Link to="/">Home</Link>
        </li>
        <li>
          <Link to="/cart">Cart</Link>
        </li>
        <li>
          <Link to="/product">Product</Link>
        </li>
        <li>
          <Link to="/profile">Profile</Link>
        </li>
      </ul>
    </nav>
  );
};

export default Navbar;
