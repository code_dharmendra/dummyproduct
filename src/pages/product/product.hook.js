import { useQuery } from "@tanstack/react-query";
import { KEYS } from "../../api/handlers";
import { getProduct, get_product_by_search } from "../../api/services/products";

const useProduct = (props) => {
  const {
    isLoading,
    data: productData,
    refetch: productRefetch,
  } = useQuery({
    queryKey: [KEYS.PRODUCT],
    queryFn: getProduct,
    refetchOnWindowFocus: false,
  });
  const { isLoading: searchLoading, data: productSearchData } = useQuery({
    queryKey: [KEYS.SINGLE_PRODUCT, props?.query ?? ""],
    queryFn: () => get_product_by_search(props?.query ?? ""),
    refetchOnWindowFocus: false,
  });
  return {
    productData,
    isLoading,
    productRefetch,
    productSearchData,
    searchLoading,
  };
};

export default useProduct;
