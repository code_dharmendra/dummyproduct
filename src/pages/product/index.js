import React, { useEffect, useState } from "react";
import ProductCard from "../../components/productCard";
import useProduct from "./product.hook";
import { Col, Row, Container, Button } from "react-bootstrap";
import LottiePlayer from "../../components/Lottie/Player";
import { Loading } from "../../components/Lottie/lottieAnimation";

import { useDispatch } from "react-redux";
import { addToCart } from "../../redux/cartSlice";

const Product = () => {
  const [searchItem, setSearchItem] = useState("");
  const { productSearchData, searchLoading, productRefetch } = useProduct({
    query: searchItem,
  });
  useEffect(() => {
    productRefetch();
  }, [searchItem]);

  const handleSearch = () => {
    productRefetch();
  };

  const handleClear = () => {
    setSearchItem("");
    productRefetch();
  };

  const dispatch = useDispatch();

  const handleAddToCart = (item) => {
    dispatch(addToCart(item));
  };

  return (
    <Container className="py-4">
      <div>
        <h1 className="mb-5 text-center">Search Product in the List</h1>
      </div>
      <div className="mb-4 d-flex flex-grow-1">
        <input
          placeholder="Search product name..."
          value={searchItem}
          onChange={(e) => setSearchItem(e.target.value)}
          className="w-100 rounded-1 fs-3 text-success px-3"
        />
        <Button
          className="bg-success border-success"
          onClick={() => handleSearch()}
        >
          Search
        </Button>
        <Button
          className="bg-danger border-danger"
          onClick={() => handleClear()}
        >
          Clear
        </Button>
      </div>
      {!searchLoading ? (
        <Row className="gy-4">
          {productSearchData?.products?.length > 1 ? (
            productSearchData?.products?.map((item, index) => {
              console.log(item, "item");
              return (
                <Col md={4} key={index}>
                  <ProductCard data={item} addToCart={handleAddToCart(item)} />
                </Col>
              );
            })
          ) : (
            <div className="h-100 centered">
              <h1 className="text-center">
                Does not exist in product list. Search any product like: iPhone
                9
              </h1>
            </div>
          )}
        </Row>
      ) : (
        <div className="h-100 centered">
          <h1 className="text-center">Loading...</h1>
        </div>
      )}
    </Container>
  );
};

export default Product;
