import React from "react";
import { useParams } from "react-router-dom";
import useDocumentTitle from "../../../function/pageTitle/useDocumentTitle";

const Productdetails = () => {
  const { id, title } = useParams();
  console.log(id, "id");
  console.log(title, "title");
  useDocumentTitle(` ${id} 👻`);

  return (
    <div>
      <h2>Project Details</h2>
      <p>Title: {id}</p>
      <p>id: {decodeURIComponent(title)}</p>
    </div>
  );
};

export default Productdetails;
