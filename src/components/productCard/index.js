import React from "react";
import { Card, Image, Button } from "react-bootstrap";
import { MdOutlineCurrencyRupee } from "react-icons/md";
import { connect } from "react-redux";
import {
  addToCart,
  removeFromCart,
  incrementQuantity,
  decrementQuantity,
} from "../../redux/cartSlice";

const ProductCard = ({
  data,
  cart,
  addToCart,
  removeFromCart,
  incrementQuantity,
  decrementQuantity,
}) => {
  const cartItem = cart.find((item) => item.id === data.id);
  const quantity = cartItem ? cartItem.quantity : 0;

  const truncateDescription = (description) =>
    description.length > 100
      ? `${description.substring(0, 100)}...`
      : description;

  const handleAddToCart = () => {
    addToCart(data);
  };

  const handleRemoveFromCart = () => {
    removeFromCart(data.id);
  };

  const handleIncrementQuantity = () => {
    incrementQuantity(data.id);
  };

  const handleDecrementQuantity = () => {
    decrementQuantity(data.id);
  };

  return (
    <Card
      className="px-2 py-4 shadow-sm"
      style={{ minHeight: 400, height: "100%" }}
    >
      <div className="d-flex flex-column gap-3">
        <h5>{data?.title ?? ""}</h5>
        <Image
          src={data?.thumbnail}
          className="img-fluid"
          style={{ height: 200 }}
        />
        <p>{truncateDescription(data?.description ?? "")}</p>
        <div className="d-flex gap-3 justify-content-between align-items-center">
          <div className="d-flex gap-3">
            <span>
              <b>Price:</b>
              <MdOutlineCurrencyRupee /> {Number(data?.price).toFixed() ?? ""}
            </span>
            <span>
              <b>Discount:</b>{" "}
              {Number(data?.discountPercentage).toFixed() ?? ""}%
            </span>
          </div>
          {quantity > 0 ? (
            <div className="d-flex gap-3">
              <Button
                variant="outline-secondary"
                onClick={handleDecrementQuantity}
              >
                -
              </Button>
              <span>{quantity}</span>
              <Button
                variant="outline-secondary"
                onClick={handleIncrementQuantity}
              >
                +
              </Button>
              <Button variant="danger" onClick={handleRemoveFromCart}>
                Remove
              </Button>
            </div>
          ) : (
            <Button onClick={handleAddToCart}>Add To Cart</Button>
          )}
        </div>
      </div>
    </Card>
  );
};

const mapStateToProps = (state) => ({
  cart: state.cart.cartItems,
});

export default connect(mapStateToProps, {
  addToCart,
  removeFromCart,
  incrementQuantity,
  decrementQuantity,
})(ProductCard);
