import React from "react";
import Lottie from "react-lottie";
import { Loading } from "./lottieAnimation";

const LottiePlayer = () => {
  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: Loading,
  };

  return <Lottie options={defaultOptions} height={200} width={200} />;
};

export default LottiePlayer;
