const generateColorWithOpacity = (hexColor, opacity) => {
  opacity = Math.min(1, Math.max(0, opacity));

  // Convert the hexadecimal color to RGB format
  let r = parseInt(hexColor.slice(1, 3), 16);
  let g = parseInt(hexColor.slice(3, 5), 16);
  let b = parseInt(hexColor.slice(5, 7), 16);

  return `rgba(${r}, ${g}, ${b}, ${opacity})`;
};

export { generateColorWithOpacity };
